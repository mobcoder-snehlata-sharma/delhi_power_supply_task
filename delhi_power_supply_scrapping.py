from datetime import datetime, timezone
from dateutil import tz
import re

import psycopg2
import requests
from bs4 import BeautifulSoup
import os
from dotenv import load_dotenv

load_dotenv()


url = "https://www.delhisldc.org/Redirect.aspx?Loc=0804"
requested_page = requests.get(url)

soup_content = BeautifulSoup(requested_page.content, "html.parser")
requested_table_id_pattern = r"ContentPlaceHolder3_*"

conn = psycopg2.connect(
    database = os.environ.get("DB_NAME"),
    host = os.environ.get("DB_HOST"),
    port=os.environ.get("DB_PORT"),
    user=os.environ.get("DB_USER"),
    password = os.environ.get("DB_PASSWORD")
)


def create_table():  

    cursor = conn.cursor() 

    delhi_export_table_column = ""

    for table in soup_content.find_all('table'): 

        table_id = str(table.get("id"))

        sql_fields_string=""

        if re.match(requested_table_id_pattern, table_id) and table_id != "ContentPlaceHolder3_dEXPORT": # excluding ContentPlaceHolder3_dEXPORT table because this table has no header

            table_name = table_id.split("_")[1]

            for table_row in table.find("tr"):

                column_data = table_row.text.strip()
        
                if column_data:

                    column_data = re.sub('[^a-zA-Z0-9 \n\.]', '_', column_data)
                    
                    column_data = re.sub(r"\s+", '_', column_data)
                    
                    sql_fields_string += f"{column_data} TEXT, "
            
            if table_id == "ContentPlaceHolder3_DIMPORT":

                delhi_export_table_column = sql_fields_string # saving ContentPlaceHolder3_DIMPORT table header for ContentPlaceHolder3_dEXPORT table

            if sql_fields_string:
                
                sql = """CREATE TABLE IF NOT EXISTS """ + table_name + """(""" + sql_fields_string +"""entry_time TIMESTAMP)"""

                cursor.execute(sql)

                print(f"Table Created...{table_name}")
                
                conn.commit()

    export_table = soup_content.find("table", id = "ContentPlaceHolder3_dEXPORT")

    if export_table and delhi_export_table_column:

        sql = """CREATE TABLE IF NOT EXISTS dEXPORT(""" + delhi_export_table_column +"""entry_time TIMESTAMP)""" # creating table ContentPlaceHolder3_dEXPORT explicitly because this table has no header

        cursor.execute(sql)
        
        print(f"Table Created...{table_name}")
            
        conn.commit()


def insert_data():

    cursor = conn.cursor()

    from_zone = tz.tzutc()

    timestamp_tag_text = soup_content.find("span", id="ContentPlaceHolder3_LblDate").text # Extracting Timestamp text

    print("Insertion started...")
    
    for table in soup_content.find_all('table'): # finding all tables present in page
        
        table_id = str(table.get("id"))

        table_name_dict = {}

        table_rows = [[ele.text.strip() for ele in item.find_all("td")] for item in table.find_all("tr")] # fetching out table rows

        if re.match(requested_table_id_pattern, table_id) and table_id != "ContentPlaceHolder3_dEXPORT":
            
            table_rows.pop(0) #popping out header row
            
            table_name_dict[table_id.split("_")[1]] = table_rows #appending dictionary of data

        if table_id == "ContentPlaceHolder3_dEXPORT":
            
            table_name_dict[table_id.split("_")[1]] = table_rows # appending dictionary Only for ContentPlaceHolder3_dEXPORT

        
        if table_name_dict:

            table_name = list(table_name_dict.keys())[0]
            
            table_data = list(table_name_dict.values())[0]

            for row in table_data:
                
                timestamp = f'{timestamp_tag_text.split(" ")[2]} {timestamp_tag_text.split(" ")[3]}' # Spliting date and time from timestamp text

                utc = datetime.strptime(timestamp, '%d/%m/%Y %H:%M:%S')

                utc = utc.replace(tzinfo=from_zone) # Converting to local timezone

                row.append(datetime.strftime(utc,"%Y-%m-%d, %H:%M:%S"))
            
            table_data = list(tuple(sub) for sub in table_data)
            
            table_data = ', '.join(map(str,table_data))

            cursor.execute("""INSERT INTO """ + table_name +""" VALUES {}""".format(table_data))
            
            conn.commit()

            print(f"Data Inserted Successfully ... {table_name}")


create_table()
insert_data()

conn.close()