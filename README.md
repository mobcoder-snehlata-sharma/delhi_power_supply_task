# delhi_power_supply_task



Pre-requisites : Install below project dependencies in system
1. Python3.7
2. Postgresql
3. Beautifulsoup4
4. Requests

Follow below steps to setup the project-


Create a virtual environment using
- python3 -m venv <virtual_env_name>


Activate the virtual environment-
- source <virtual_env_name>/bin/activate


Install requirement.txt available in project.
- pip install -r requirements.txt


Follow below steps run and insert data-


Run command to creates and insert data into table.
- python3 delhi_power_supply_scrapping.py
